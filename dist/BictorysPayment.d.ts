export declare class BictorysPayment {
    private initObject;
    _name: string;
    _age: number | null;
    constructor(initObject: InitType);
    init(): string;
}
type InitType = {
    name: string;
    age: number | null;
};
export {};
